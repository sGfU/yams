version: "3"

services:
  # <media_service> is used to serve your media to the client devices
  <media_service>:
    image: lscr.io/linuxserver/${MEDIA_SERVICE}
    container_name: ${MEDIA_SERVICE}
    #network_mode: host # plex
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
      - VERSION=docker
    volumes:
      - ${MEDIA_DIRECTORY}/movies:/data/movies
      - ${MEDIA_DIRECTORY}/tvshows:/data/tvshows
      - ${INSTALL_DIRECTORY}/config/${MEDIA_SERVICE}:/config
    ports: # plex
      - 8096:8096 # plex
    restart: unless-stopped

  # qBitorrent is used to download torrents
  qbittorrent:
    image: lscr.io/linuxserver/qbittorrent:4.6.0
    container_name: qbittorrent
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
      - WEBUI_PORT=8080
    volumes:
      - ${MEDIA_DIRECTORY}/downloads:/data/downloads
      - ${INSTALL_DIRECTORY}/config/qbittorrent:/config
    restart: unless-stopped
    ports: # qbittorrent
      - 8080:8080 # qbittorrent
    #network_mode: "service:gluetun"

  # Sonarr is used to query, add downloads to the download queue and index TV shows
  # https://sonarr.tv/
  sonarr:
    image: lscr.io/linuxserver/sonarr
    container_name: sonarr
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
    volumes:
      - ${MEDIA_DIRECTORY}:/data # Pass through the entire /data so sonarr can treat /data/downloads and /data/tvshows as the same directory
#      - ${MEDIA_DIRECTORY}/downloads:/downloads # No longer required for hardlinking to function per TRaSH Guides
      - ${INSTALL_DIRECTORY}/config/sonarr:/config
    ports:
      - 8989:8989
    restart: unless-stopped

  # Radarr is used to query, add downloads to the download queue and index Movies
  # https://radarr.video/
  radarr:
    image: lscr.io/linuxserver/radarr
    container_name: radarr
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
    volumes:
      - ${MEDIA_DIRECTORY}:/data # Pass through the entire /data so radarr can treat /data/downloads and /data/tvshows as the same directory
#      - ${MEDIA_DIRECTORY}/downloads:/downloads # No longer required for hardlinking to function per TRaSH Guides
      - ${INSTALL_DIRECTORY}/config/radarr:/config
    ports:
      - 7878:7878
    restart: unless-stopped

  # Lidarr is used to query, add downloads to the download queue and index Music
  # https://lidarr.audio/
  lidarr:
    image: lscr.io/linuxserver/lidarr
    container_name: lidarr
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
    volumes:
      - ${MEDIA_DIRECTORY}:/data # Pass through the entire /data so lidarr can treat /data/downloads and /data/tvshows as the same directory
#      - ${MEDIA_DIRECTORY}/downloads:/downloads # No longer required for hardlinking to function per TRaSH Guides
      - ${INSTALL_DIRECTORY}/config/lidarr:/config
    ports:
      - 8686:8686
    restart: unless-stopped

  # Readarr is used to query, add downloads to the download queue and index Audio and Ebooks
  # https://readarr.com/
  readarr:
    image: lscr.io/linuxserver/readarr:develop
    container_name: readarr
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
    volumes:
      - ${MEDIA_DIRECTORY}:/data # Pass through the entire /data so readarr can treat /data/downloads and /data/tvshows as the same directory
#      - ${MEDIA_DIRECTORY}/downloads:/downloads # No longer required for hardlinking to function per TRaSH Guides
      - ${INSTALL_DIRECTORY}/config/readarr:/config
    ports:
      - 8787:8787
    restart: unless-stopped

  # Bazarr is used to download and categorize subtitles
  # https://www.bazarr.media/
  bazarr:
    image: lscr.io/linuxserver/bazarr
    container_name: bazarr
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
    volumes:
      - ${MEDIA_DIRECTORY}:/data/media # I don't know tbh this is just what TRaSH Guides recommends
#      - ${MEDIA_DIRECTORY}/tvshows:/tv # Not needed for hardlinking
      - ${INSTALL_DIRECTORY}/config/bazarr:/config
    ports:
      - 6767:6767
    restart: unless-stopped

  # Prowlarr is our torrent indexer/searcher. Sonarr/Radarr use Prowlarr as a source
  # https://prowlarr.com/
  prowlarr:
    image: lscr.io/linuxserver/prowlarr
    container_name: prowlarr
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
    volumes:
      - ${INSTALL_DIRECTORY}/config/prowlarr:/config
    ports:
      - 9696:9696
    restart: unless-stopped

  # Gluetun is our VPN, so you can download torrents safely
  gluetun:
    image: qmcgaw/gluetun:v3
    container_name: gluetun
    cap_add:
      - NET_ADMIN
    devices:
      - /dev/net/tun:/dev/net/tun
    ports:
      - 8888:8888/tcp # HTTP proxy
      - 8388:8388/tcp # Shadowsocks
      - 8388:8388/udp # Shadowsocks
      #- 8080:8080/tcp # gluetun
    volumes:
      - ${INSTALL_DIRECTORY}/config/gluetun:/config
    environment:
      - VPN_SERVICE_PROVIDER=${VPN_SERVICE}
      - VPN_TYPE=openvpn
      - OPENVPN_USER=${VPN_USER}
      - OPENVPN_PASSWORD=${VPN_PASSWORD}
      - OPENVPN_CIPHERS=AES-256-GCM
    restart: unless-stopped

  # Portainer helps debugging and monitors the containers
  portainer:
    image: portainer/portainer-ce
    container_name: portainer
    ports:
      - 9000:9000
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - ${INSTALL_DIRECTORY}/config/portainer:/data
    restart: unless-stopped

  # Watchtower is going to keep our instances updated
  watchtower:
    image: containrrr/watchtower
    container_name: watchtower
    environment:
      - WATCHTOWER_CLEANUP=true
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    restart: unless-stopped
